package com.taliro.quickdev.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.taliro.quickdev.common.service.AbstractFilter;

import java.io.IOException;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class EntityUtils {

    public static final String FILTER_FIELDS_BY_NAME = "filterFieldsByName";

    public static Map<String, Object> checkFields(Map<String, Object> record) {
        return record
            .entrySet()
            .stream()
            .filter(it -> !List.of("id", "updated", "created").contains(it.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @SuppressWarnings("unchecked")
    public static <T> T merge(T first, Map<String, Object> second) throws IOException {
        var mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        second = EntityUtils.checkFields(second);
        Map<String, Object> map = mapper.convertValue(first, Map.class);
        ObjectReader objectReader = mapper.readerForUpdating(map);
        Map<String, Object> newMap = objectReader.readValue((JsonNode) mapper.valueToTree(second));
        return (T) mapper.convertValue(newMap, first.getClass());
    }

    @SuppressWarnings("unchecked")
    public static <T> T merge(T first, T second) throws IOException {
        var mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        Map<String, Object> map = mapper.convertValue(second, Map.class);
        return merge(first, map);
    }

    public static <T> List<T> merge(Collection<T> first, Collection<T> second, BiFunction<T, T, Boolean> comparator) {
        List<T> list = new ArrayList<>();

        if (second == null) {
            return new ArrayList<>();
        }

        for (T t : second) {
            boolean founded = false;
            for (T t1 : first) {
                if (comparator.apply(t1, t)) {
                    list.add(t1);
                    founded = true;
                }
            }

            if (!founded) {
                list.add(t);
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public static <T> Map<String, Object> toMap(T entity, List<String> fields) {
        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        FilterProvider filterProvider;
        if (fields != null && !fields.isEmpty()) {
            filterProvider =
                new SimpleFilterProvider()
                    .addFilter(
                        FILTER_FIELDS_BY_NAME,
                        (PropertyFilter) SimpleBeanPropertyFilter.filterOutAllExcept(new HashSet<>(fields))
                    );
        } else {
            filterProvider =
                new SimpleFilterProvider()
                    .addFilter(FILTER_FIELDS_BY_NAME, (PropertyFilter) SimpleBeanPropertyFilter.serializeAll());
        }
        mapper.addMixIn(entity.getClass(), AbstractFilter.class);
        return (Map<String, Object>) mapper.setFilterProvider(filterProvider).convertValue(entity, Map.class);
    }
}
