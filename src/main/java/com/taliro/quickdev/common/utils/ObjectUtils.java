package com.taliro.quickdev.common.utils;

import java.math.BigDecimal;

public final class ObjectUtils {

    public static Object cast(Class<?> caster, Object value) {
        if (caster.getName().equals(Long.class.getName())) {
            return ((Integer) value).longValue();
        } else if (caster.getName().equals(BigDecimal.class.getName())) {
            return new BigDecimal((Integer) value);
        } else {
            return value;
        }
    }
}
