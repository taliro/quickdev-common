package com.taliro.quickdev.common.utils;

import com.taliro.quickdev.common.exception.CDIFailedException;
import jakarta.enterprise.inject.spi.CDI;

public final class Beans {

    @SuppressWarnings("unchecked")
    public static <T> T get(Class<T> clazz) {
        try {
            return CDI.current().select(clazz).get();
        } catch (Exception e) {
            throw new CDIFailedException("Cannot found class %s".formatted(clazz.getName()), e);
        }
    }
}
