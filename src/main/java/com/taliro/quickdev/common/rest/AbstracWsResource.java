package com.taliro.quickdev.common.rest;

import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("/ws")
public abstract class AbstracWsResource {}
