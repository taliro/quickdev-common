package com.taliro.quickdev.common.exception;

public class CDIFailedException extends RuntimeException {

    public CDIFailedException(String message) {
        super(message);
    }

    public CDIFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
