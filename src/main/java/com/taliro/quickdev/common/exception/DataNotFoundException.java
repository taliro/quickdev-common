package com.taliro.quickdev.common.exception;

public class DataNotFoundException extends RuntimeException {

    public DataNotFoundException(String message) {
        super(message + " non trouvé");
    }
}
