package com.taliro.quickdev.common.exception;

public class UnauthorizeAuthentificationException extends RuntimeException {

    public UnauthorizeAuthentificationException(String message) {
        super(message);
    }

    public UnauthorizeAuthentificationException(String message, Throwable cause) {
        super(message, cause);
    }
}
