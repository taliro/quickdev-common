package com.taliro.quickdev.common.exception;

public class InvalidBuilderException extends RuntimeException {

    public InvalidBuilderException(String message) {
        super(message);
    }

    public InvalidBuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
