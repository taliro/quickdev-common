package com.taliro.quickdev.common.exception;

public class PlatformException extends RuntimeException {

    public PlatformException() {}

    public PlatformException(String message) {
        super(message);
    }

    public PlatformException(String message, Throwable cause) {
        super(message, cause);
    }
}
