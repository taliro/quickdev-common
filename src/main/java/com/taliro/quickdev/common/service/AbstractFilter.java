package com.taliro.quickdev.common.service;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.taliro.quickdev.common.utils.EntityUtils;

@JsonFilter(EntityUtils.FILTER_FIELDS_BY_NAME)
public abstract class AbstractFilter {}
