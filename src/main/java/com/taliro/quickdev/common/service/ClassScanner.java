package com.taliro.quickdev.common.service;

import com.taliro.quickdev.common.exception.ResourceException;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.Entity;
import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class ClassScanner {

    public Set<Class<?>> getEntities() {
        String[] packageName = getClass().getName().split("\\.");
        Reflections reflections = new Reflections(packageName[0]);
        return reflections.getTypesAnnotatedWith(Entity.class);
    }

    @SuppressWarnings("unchecked")
    public <T> Class<T> getEntity(String model) {
        return (Class<T>) getEntities()
            .stream()
            .filter(it -> it.getName().equals(model))
            .findFirst()
            .orElseThrow(() -> new ResourceException("cannot found model %s".formatted(model)));
    }

    @SuppressWarnings("rawtypes")
    public Set<Class<? extends PanacheRepository>> getRepositories() {
        String[] packageName = getClass().getName().split("\\.");
        Reflections reflections = new Reflections(packageName[0]);
        return reflections
            .getSubTypesOf(PanacheRepository.class)
            .stream()
            .filter(it -> !Modifier.isAbstract(it.getModifiers()))
            .collect(Collectors.toSet());
    }

    @SuppressWarnings("unchecked")
    public <T> Class<? extends PanacheRepository<T>> getRepository(String model) {
        return (Class<? extends PanacheRepository<T>>) getRepositories()
            .stream()
            .filter(it -> findRepositoryByModel(it, model))
            .findFirst()
            .orElseThrow(() -> new ResourceException("cannot found model %s".formatted(model)));
    }

    @SuppressWarnings("rawtypes")
    private boolean findRepositoryByModel(Class<? extends PanacheRepository> clazz, String model) {
        Type[] genericInterfaces = clazz.getGenericInterfaces();
        for (Type interfaceClass : genericInterfaces) {
            if (interfaceClass instanceof ParameterizedType parameterizedType) {
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                if (actualTypeArguments.length > 0) {
                    Class<?> genericClass = (Class<?>) actualTypeArguments[0];
                    return model.equals(genericClass.getName());
                }
            }
        }
        return false;
    }
}
