package com.taliro.quickdev.common.service;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.lang.reflect.Field;

public class ChildSerializer extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, com.fasterxml.jackson.core.JsonGenerator gen, SerializerProvider serializers)
        throws IOException {
        try {
            gen.writeStartObject(gen.getCurrentValue());
            Field id = value.getClass().getDeclaredField("id");
            id.setAccessible(true);
            gen.writeNumberField("id", (Long) id.get(value));
            gen.writeEndObject();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
