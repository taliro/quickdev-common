package com.taliro.quickdev.common.service;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Arrays;
import java.util.List;

@Converter
public class StringToListStringConverter implements AttributeConverter<List<String>, String> {

    @Override
    public String convertToDatabaseColumn(List<String> strings) {
        if (strings == null) return null;
        return String.join(",", strings);
    }

    @Override
    public List<String> convertToEntityAttribute(String joined) {
        if (joined == null) return null;
        return Arrays.asList(joined.split(","));
    }
}
