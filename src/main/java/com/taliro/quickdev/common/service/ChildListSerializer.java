package com.taliro.quickdev.common.service;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;

public class ChildListSerializer extends JsonSerializer<Collection<Object>> {

    @Override
    public void serialize(Collection<Object> list, com.fasterxml.jackson.core.JsonGenerator gen, SerializerProvider serializers)
        throws IOException {
        try {
            if (list == null) {
                return;
            }

            gen.writeStartArray(gen.getCurrentValue());
            for (Object o : list) {
                Field id = o.getClass().getDeclaredField("id");
                id.setAccessible(true);
                Object idObj = id.get(o);

                if (idObj == null) {
                    continue;
                }

                gen.writeStartObject();
                gen.writeNumberField("id", (Long) idObj);
                gen.writeEndObject();
            }
            gen.writeEndArray();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
