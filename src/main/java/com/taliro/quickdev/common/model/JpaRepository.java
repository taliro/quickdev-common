package com.taliro.quickdev.common.model;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

public interface JpaRepository<T extends AbstractModel> extends PanacheRepository<T> {
    default void save(T t) {
        if (t.getId() == null) {
            persist(t);
        } else {
            getEntityManager().merge(t);
        }
    }
}
